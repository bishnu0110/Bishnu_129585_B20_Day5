
<?php
//str_shuffle — Randomly shuffles a string

$str = 'abcdef';
$shuffled = str_shuffle($str);

// This will echo something like: bfdaec
echo $shuffled;

?>
